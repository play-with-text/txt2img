#!/usr/bin/env python

import argparse
import random
from PIL import Image
import math 

def main():
    # read commandline arguments, first
    parser = argparse.ArgumentParser(description="convert a text to a pixel art image")

    parser.add_argument('toOpen', metavar='N', type=str, 
                        help='File text to convert to img')
    
    parser.add_argument('-S', '--show', required=False, action='store_true',
                        help="display generated image")
    
    parser.add_argument('-o', '--output', required=False,
                        help="Save file with given name to PNG")
    
    # Parse Args
    args = parser.parse_args()
    print(args.toOpen)
    letterColors = {}
    pixelList = []

    with open(args.toOpen, 'r') as f:
        while True:
            char = None
            try:
                # read char one by one
                char = f.read(1)
            except UnicodeDecodeError:
                print("File Error: " +
                        f + ' is not Raw Text')
                break
            except IOError:
                print("IO File Error: " + f)
                break
            if char is not None:
                # If not end of file
                if char != '':
                    if char.isalpha():
                        if char in letterColors:
                            pixelList.append(letterColors[char])
                        else:
                            r = random.randint(0,255)
                            g = random.randint(0,255)
                            b = random.randint(0,255)
                            rgb = (r,g,b)
                            letterColors.update({char: rgb})
                            pixelList.append(letterColors[char])
                # End Of File
                else:
                    break
    nbPixel = len(pixelList)
    height = width = int(math.sqrt(nbPixel))
    nbPixel = width*height
    
    img = Image.new('RGB', (width, height))
    
    y = 0
    i = 0
    while i < nbPixel:
        for x in range(width):
            img.putpixel((x,y), pixelList[i])
            i += 1
        y += 1
    if args.show is True:
        img.show()
    if args.output:
        img.save(args.output + '.png', "PNG") 

    exit()

if __name__ == "__main__":
    main()
